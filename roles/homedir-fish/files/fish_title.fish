function fish_title
    if set -q INSIDE_EMACS
        return
    end

    echo $HOSTNAME $USER (tty)
end
