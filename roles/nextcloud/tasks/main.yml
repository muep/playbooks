- name: Install dependencies
  package:
    name: "{{ item }}"
    state: present
  with_items:
    - php
    - php-gd
    - php-intl
    - php-json
    - php-mbstring
    - php-opcache
    - php-pgsql
    - php-process
    - php-sodium
    - php-xml
    - php-zip
  tags:
    - package

- name: Add nextcloud group
  group:
    system: yes
    name: nextcloud
    state: present

- name: Add nextcloud user
  user:
    system: yes
    name: nextcloud
    group: nextcloud
    state: present

- name: SELinux rules for marking directory as writable
  sefcontext:
    target: "{{ item }}(/.*)?"
    setype: httpd_sys_rw_content_t
    state: present
  with_items:
    - /var/www/nextcloud
    - /var/www/nextcloud-data

- name: Download nextcloud
  get_url:
    url: "https://download.nextcloud.com/server/releases/nextcloud-{{ nextcloud_version }}.zip"
    dest: "/var/www/nextcloud-{{ nextcloud_version }}.zip"
    checksum: "{{ nextcloud_checksum[nextcloud_version] }}"

- name: Unpack nextcloud
  unarchive:
    dest: /var/www
    src: "/var/www/nextcloud-{{ nextcloud_version }}.zip"
    remote_src: yes
    creates: /var/www/nextcloud
    owner: nextcloud
    group: nextcloud

- name: Nextcloud data directory
  file:
    path: /var/www/nextcloud-data
    state: directory
    owner: nextcloud
    group: nextcloud
    mode: '750'

- name: Nextcloud php directories
  file:
    path: "/var/lib/php/{{ item }}/"
    state: directory
    owner: nextcloud
    group: nextcloud
    mode: '700'
  with_items:
    - opcache-nextcloud
    - session-nextcloud

- name: nextcloud php-fpm config
  copy:
    src: files/php-fpm-nextcloud.conf
    dest: /etc/php-fpm.d/nextcloud.conf
    owner: root
    group: root
    mode: '644'
  notify:
    - restart php-fpm

- name: nextcloud httpd conf
  copy:
    src: files/httpd-nextcloud.conf
    dest: /etc/httpd/conf.d/00-nextcloud.conf
    owner: root
    group: root
    mode: '644'
  notify:
    - restart httpd

- name: postgresql nextcloud user
  become_user: postgres
  postgresql_user:
    name: nextcloud
    state: present
  tags:
    - postgresql

- name: postgresql nextcloud database
  become_user: postgres
  postgresql_db:
    name: nextcloud
    owner: nextcloud
  tags:
    - postgresql
