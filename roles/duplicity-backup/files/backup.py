#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# backup.py - helper script for driving duplicity-backed full
# system backup
#
# Copyright (c) 2016, Joonas Sarajärvi <muep@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


import argparse
import collections
try:
    # Python 3
    import configparser as cp
except ImportError:
    # Python 2
    import ConfigParser as cp
import os
import os.path
import sys


EXCLUDED_FSES = {
    "cifs",
    "nfs",
    "nfs4",
    "proc",
    "sysfs",
    "tmpfs",
}


EXCLUDED_PATHS = {
    "**/.cache",
    "**/.gvfs",
    "/dev",
    "/lost+found",
    "/media"
    "/mnt",
    "/proc",
    "/run",
    "/selinux",
    "/sys",
    "/tmp",
    "/var/cache/dnf",
    "/var/cache/PackageKit",
    "/var/cache/yum",
    "/var/lib/libvirt/images",
    "/var/lock",
    "/var/run",
    "/var/tmp",
}


Config = collections.namedtuple("Config",
                                ("destination",
                                 "excludes",
                                 "gpgkey",
                                 "id_rsa_path",
                                 ))


Mount = collections.namedtuple("Mount", ("path", "type"))


def all_excludes_for_config(c):
    assert isinstance(c, Config)

    return c.excludes | EXCLUDED_PATHS | dynamic_excludes()


def argument_parser():
    p = argparse.ArgumentParser()

    p.add_argument("config")
    p.add_argument("--dry-run", action="store_true")
    p.add_argument("--allow-source-mismatch",
                   action="store_true",
                   help="passed on to duplicity")

    return p


def config_from_path(config_path):
    parser = cp.ConfigParser()

    files_read = parser.read(config_path)
    if not files_read:
        raise Exception("Failed to read {}".format(config_path))

    destination = parser.get("DEFAULT", "destination")
    id_rsa_path = parser.get("DEFAULT", "ssh_id_rsa")

    try:
        gpgkey = parser.get("DEFAULT", "gpgkey")
    except cp.NoOptionError:
        gpgkey = None

    excludes = unpack_excludes(parser.get("DEFAULT", "exclude"))

    return Config(destination=destination,
                  excludes=excludes,
                  gpgkey=gpgkey,
                  id_rsa_path=id_rsa_path)


def dynamic_excludes():
    items = []

    with open("/proc/mounts", "r") as f:
        for mount in map(mount_from_mountline, f):
            assert isinstance(mount, Mount)

            if mount.type in EXCLUDED_FSES:
                items.append(mount.path)

    return set(items)


def exclude_args_from_excludes(excludes):
    return ["--exclude={}".format(i) for i in sorted(excludes)]


def id_rsa_arg(id_rsa_path):
    return "--ssh-options=-oIdentityFile={}".format(id_rsa_path)


def main():
    parser = argument_parser()

    parsed_args = parser.parse_args()

    config = config_from_path(parsed_args.config)

    excludes = all_excludes_for_config(config)

    dup_cmd = [
        "duplicity", "incremental",
        "/", config.destination,
        "--full-if-older-than", "1M",
    ]

    dup_cmd += exclude_args_from_excludes(excludes)

    if config.id_rsa_path:
        dup_cmd.append(id_rsa_arg(config.id_rsa_path))

    if not config.gpgkey:
        dup_cmd.append("--no-encryption")

    if parsed_args.allow_source_mismatch:
        dup_cmd.append("--allow-source-mismatch")

    if parsed_args.dry_run:
        print(dup_cmd[0], "\\")
        for item in dup_cmd[1:-1]:
            print("  {} \\".format(item))
        print("  {}".format(dup_cmd[-1]))
    else:
        env = {k: os.environ[k] for k in os.environ}
        if config.gpgkey:
            env["PASSPHRASE"] = config.gpgkey
        os.execvpe("duplicity", dup_cmd, env)


def mount_from_mountline(line):
    items = line.split()
    assert len(items) == 6

    return Mount(path=items[1], type=items[2])


def unpack_excludes(exclude_txt):
    return {item for item in exclude_txt.split("\n") if item}


if __name__ == "__main__":
    sys.exit(main())
