#!/bin/sh

set -x
set -e

if test -e /etc/os-release
then
    os_name=$(. /etc/os-release && echo "${ID}")
    os_like=$(. /etc/os-release && echo "${ID_LIKE}")
    os_ver=$(. /etc/os-release && echo "${VERSION_ID}")
else
    echo "Could not detect OS type"
    exit 1
fi

if test fedora = "${os_name}"
then
    /usr/bin/dnf -y remove initial-setup
    # Assuming a recent Fedora installation
    /usr/bin/dnf -y update
    /usr/bin/dnf -y install \
         python3 \
         python3-dnf \
         python3-libselinux \
         python3-libsemanage

    # No need to run the interactive initial setup tool. This is
    # sometimes there, e.g. in the ARM images.
    systemctl disable initial-setup || echo "ignoring failure"
    systemctl stop initial-setup || echo "ignoring failure"
elif test centos = "${os_name}" -a 9 = "${os_ver}"
then
    # CentOS 9 should be pretty good to go out of the box
    dnf -y update
elif test centos = "${os_name}" -a 8 = "${os_ver}"
then
    # CentOS 8 should be pretty good to go out of the box
    /usr/bin/dnf -y install python3-policycoreutils
elif test centos = "${os_name}" -a 7 = "${os_ver}"
then
    # CentOS 7
    /usr/bin/yum -y update
    /usr/bin/yum -y install \
        hostname \
        libsemanage-python
elif test debian = "${os_name}" -o debian = "${os_like}"
then
    /usr/bin/apt-get update
    /usr/bin/apt-get -y dist-upgrade
    /usr/bin/apt-get -y install python3 python3-apt
else
    echo "Unexpected OS type"
    exit 1
fi

touch /etc/bootstrap-done
