#!/bin/sh

domains="${1}"
email="${2}"

exec certbot certonly --webroot -n --agree-tos \
     -d "${domains}" -m "${email}" -w /var/www/html
