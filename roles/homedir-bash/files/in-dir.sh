#!/bin/sh

if test "${#}" -lt 2
then
    printf "usage: %s <directory> <command> [args..]\n" "${0}"
    exit 1
fi

set -e

destdir="${1}"
shift
cmd="${1}"
shift

cd "${destdir}"
exec "${cmd}" "${@}"
