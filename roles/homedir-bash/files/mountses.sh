#!/bin/sh

exec mount|egrep -v 'type (cgroup|sysfs|proc|devtmpfs|securityfs|tmpfs|devpts|pstore|selinuxfs|configfs|mqueue|debugfs|autofs|tracefs|efivarfs|hugetlbfs|binfmt_misc|rpc_pipefs|fuse|nsfs)'
