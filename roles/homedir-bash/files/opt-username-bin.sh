# The usual place for self-compiled software.
if test -d /opt/${USER}/bin
then
    path_prepend /opt/${USER}/bin
fi

# This is used on some hosts where a user specific directory under
# /opt seemed questionable.
if test -d "${HOME}"/opt/bin
then
    path_prepend "${HOME}"/opt/bin
fi
