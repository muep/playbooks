#!/bin/sh
#
# This is a small wrapper that runs some terminal-based text editor,
# based on what is available.

if test $# -gt 1
then
    printf "usage: %s [FILENAME]\n" "${0}" 1>&2
    exit 2
fi

if command -v emacs > /dev/null 2>&1
then
    qinit=~/.emacs.d/quick-init.el
    if test -e "${qinit}"
    then
        editor="emacs -nw -q --load ${qinit}"
    else
        editor="emacs -nw"
    fi
elif command -v vim > /dev/null 2>&1
then
    editor="vim --"
elif command -v vi > /dev/null 2>&1
then
    editor="vi --"
elif command -v nano > /dev/null 2>&1
then
    editor="nano"
elif command -v ed > /dev/null 2>&1
then
    editor="ed"
else
    printf "no known editor seems to be available\n" 1>&2
    exit 3
fi

if test -z "${1}"
then
    exec ${editor}
else
    exec ${editor} "${1}"
fi
