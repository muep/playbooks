#!/bin/sh

if command -v xterm > /dev/null 2>&1
then
    exec xterm "$@"
elif command -v urxvt > /dev/null 2>&1
then
    exec urxvt "$@"
elif command -v gnome-terminal > /dev/null 2>&1
then
    exec gnome-terminal "$@"
else
    printf "no known terminal editor seems to be available\n" 1>&2
    exit 3
fi
