#!/bin/sh

if test -z "${1}"
then
    echo "usage: ${0} <ping destination>"
    exit 1
fi

ping -c1 "${1}" || exec nmcli con up id wan
