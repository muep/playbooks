#!/usr/bin/env python


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


import argparse
import collections
import os
import subprocess
import sys


class Backend(collections.namedtuple("Backend", ("apply", "get_updates"))):
    @staticmethod
    def for_apt():
        return Backend(apply=apt_apply_updates,
                       get_updates=apt_get_updates)

    @staticmethod
    def for_dummy():
        return Backend(apply=dummy_apply_updates,
                       get_updates=dummy_get_updates)

    @staticmethod
    def for_dnf():
        return Backend(apply=dnf_apply_updates,
                       get_updates=dnf_get_updates)

    @staticmethod
    def for_yum():
        return Backend(apply=yum_apply_updates,
                       get_updates=yum_get_updates)


# Simplistic view of the /etc/os-release file
class OsRelease(collections.namedtuple("OsRelease", ("name", "variant", "version"))):
    @staticmethod
    def from_etc():
        name_key = "ID"
        variant_key = "VARIANT_ID"
        version_key = "VERSION_ID"
        name = None
        variant = None
        version = None

        with open("/etc/os-release", "r") as f:
            for line in f:
                unpacked = read_osrelease_line(line)
                if unpacked is None:
                    continue

                key, value = unpacked

                if key == name_key:
                    name = value
                elif key == variant_key:
                    variant = value
                elif key == version_key:
                    version = value

        assert name is not None
        assert version is not None

        return OsRelease(name=name, variant=variant, version=version)


class UpdateItem(collections.namedtuple("UpdateItem", ("name", "version"))):
    @staticmethod
    def from_apt_item(aptitem):
        return UpdateItem(name=aptitem.candidate.package.name,
                          version=aptitem.candidate.version)

    @staticmethod
    def from_dnf_item(dnfitem):
        return UpdateItem(name=dnfitem.name,
                          version=dnfitem.evr)

    @staticmethod
    def from_yum_item(yumitem):
        return UpdateItem(name=yumitem.name,
                          version=yumitem.evr)

    def text(self):
        # Here is a nice assumption of a 80-ish column
        # terminal.
        return "{:40} {}".format(self.name, self.version)


def apt_apply_updates():
    subprocess.call(["apt-get", "update"])

    dist_upgrade = [
        "apt-get", "-y",
        "-o", "Dpkg::Options::=--force-confdef",
        "-o", "Dpkg::Options::=--force-confold",
        "dist-upgrade",
    ]
    os.execvp("apt-get", dist_upgrade)


def apt_get_updates():
    import apt

    c = apt.cache.Cache()
    try:
        c.update()
        c.open()

        # This does not yet actually alter the system unless commit()
        # is called.
        c.upgrade(dist_upgrade=True)

        updates = (UpdateItem.from_apt_item(i) for i in c.get_changes())
        return sorted(updates, key=lambda update: update.name)
    finally:
        c.close()


def dummy_apply_updates():
    pass


def dummy_get_updates():
    return []


def argument_parser():
    p = argparse.ArgumentParser()

    p.add_argument("action", choices=("apply", "list"))

    return p


def dnf_apply_updates():
    # Instead of an API call, seems safer to just make things as if
    # the admin ran the normal update command
    os.execvp("dnf", ["dnf", "-y", "update"])


def dnf_get_updates():
    import dnf

    base = dnf.Base()
    try:
        base.read_all_repos()
        base.fill_sack()

        query = base.sack.query().upgrades()

        return [UpdateItem.from_dnf_item(i) for i in query]
    finally:
        base.close()


def read_osrelease_line(line):
    line = line.strip()
    split_at = line.find("=")
    if split_at < 0:
        return None

    key = line[:split_at]
    value = line[split_at + 1:]
    for q in ("'", '"'):
        if value.startswith(q) and value.endswith(q):
            value = value[1:-1]

    if not key:
        return None

    if not value:
        return None

    return key, value


def select_backend():
    os_release = OsRelease.from_etc()
    name = os_release.name
    version = os_release.version

    if name == "fedora":
        if os_release.variant == "atomic.host":
            # Atomic host does not do traditional package
            # management.
            return Backend.for_dummy()
        else:
            return Backend.for_dnf()
    elif name == "centos" and version in ("6", "7"):
        return Backend.for_yum()
    elif name == "centos":
        return Backend.for_dnf()
    elif name in ("debian", "raspbian", "ubuntu"):
        return Backend.for_apt()
    else:
        raise Exception("No backend for {}".format(name))


def main():
    parsed_args = argument_parser().parse_args()
    backend = select_backend()

    if parsed_args.action == "list":
        for item in backend.get_updates():
            print(item.text())
    elif parsed_args.action == "apply":
        backend.apply()
    else:
        print("action not implemented!", file=sys.stderr)
        return 1

    return 0


def yum_apply_updates():
    # Instead of an API call, seems safer to just make things as if
    # the admin ran the normal update command
    os.execvp("yum", ["yum", "-y", "update"])


def yum_get_updates():
    import yum

    base = yum.YumBase()
    try:
        base.preconf.debuglevel = 0
        updates = base.doPackageLists(pkgnarrow="updates")
        return sorted((UpdateItem.from_yum_item(i) for i in updates))
    finally:
        base.close()


if __name__ == "__main__":
    sys.exit(main())
